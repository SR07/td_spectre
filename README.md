# SR07 - TD Vulnérabilités matérielles

## Introduction

Dans ce TD on va tenter de réaliser une attaque type 'Spectre' dans un environnement
contrôlé.

Cette attaque exploite une vulnérabilité matérielle présente dans de nombreux processeurs
et permet de passer outre l'isolation intra et inter processus. Comme il s'agit d'une vulnérabilité
matérielle liée au design et au principe de fonctionnement des processeurs, il est assez difficile de
s'en défendre, même si les fabricants de CPU y travaillent.

En particulier, Spectre permet de faire fuir de l'information entre *sandboxes* dans les applications qui utilisent
ce genre de mécanismes pour isoler leurs composants. Par exemple, les navigateurs web l'utilisent pour isoler les onglets
de navigation les uns des autres.

## Les références

- L'article décrivant [Spectre](https://spectreattack.com/spectre.pdf)
- L'article décrivant [Meltdown](https://meltdownattack.com/meltdown.pdf)

## Ce qu'on va voir

- Caches CPU
- Prédiction de branchement, exécution spéculative, exécution 'out-of-order' dans les CPU
- Exploitation du cache comme canal auxiliaire
- Vulnérabilité Spectre

## Récupération et compilation des codes

### Dépôt gitlab

Les codes sont accessibles sur le Gitlab de l'UTC : [https://gitlab.utc.fr/SR07/td_spectre](https://gitlab.utc.fr/SR07/td_spectre)

Pour les récupérer, clonez le dépôt comme suit :

```code bash
git clone https://gitlab.utc.fr/SR07/td_spectre.git
```

### Compilation

Tous les codes devront être compilés comme suit :

```code bash
gcc -O0 -o <binaire> <fichier.c>
```

Vous pouvez utiliser le *makefile* fourni :

```code bash
make all
```

### Exécution

Les programmes doivent être ensuite lancés somme suit :

```code bash
taskset 1 ./<binaire>
```

La commande ```taskset``` permet de définir l'affinité CPU d'un processus. Ici on fait en sorte que le processus soit
toujours exécuté sur le même cœur.

## Première partie : utilisation du cache comme canal auxiliaire

Meltdown et Spectre utilisent le cache comme canal auxiliaire pour révéler un secret protégé. La technique utilisée est
dite FLUSH + RELOAD (vidage et rechargement). On va étudier cette technique en premier.

La mémoire cache est une ressource partagée entre tous les processus. Si une donnée est présente dans le cache, son accès se fera beaucoup
plus rapidement que si elle n'est que dans la mémoire centrale.

### Organisation des caches

Sur un PC moderne, il y a trois niveaux de cache :

- Cache L1 (séparé en cache instructions et données), privé à chaque cœur, le plus proche des unités de calcul
- Cache L2, en amont du cache L1, privé à chaque cœur
- Cache L3, en amont du cache L2, unifié (commun à tous les cœurs)

![Hierarchie des caches](doc/nehalem_cache.png)

La quantité de cache varie, mais chaque ligne de cache fait exactement 64 octets : chaque bloc de 64 octets de la mémoire principale (donc avec une
adresse multiple de 64) partage le même emplacement dans le cache.

L'accès au cache est d'autant plus rapide qu'il est proche. Il existe de nombreuses optimisations de l'usage du cache, comme par exemple le chargement
spéculatif en avant ou en arrière quand le contrôleur détecte des accès mémoire séquentiels.

### Fonctions utiles

Les fonctions suivantes sont des fonctions intrinsèques (c'est à dire qu'elles correspondent à des instructions machine uniques) de
gestion du cache. Ces instructions ne sont pas privilégiées.

- Vider une ligne de cache : `_mm_clflush` : [doc intel](https://software.intel.com/sites/landingpage/IntrinsicsGuide/#text=_mm_clflush). Cette instruction invalide la ligne de cache qui contient l'adresse spécifiée en paramètre. Cette instruction prend plusieurs centaines de cycles CPU pour s'exécuter complètement.
- Lire le timestamp CPU : `__rdtscp` : [doc intel](https://software.intel.com/sites/landingpage/IntrinsicsGuide/#text=__rdtscp). Cette instruction retourne le timestamp CPU (compteur de cycles CPU, la durée d'un cycle dépend de la fréquence d'horloge CPU). On va l'utiliser pour mesurer le temps d'accès à une donnée en cache.

Exemple d'utilisation :

```C
// Vider la ligne de cache qui contient les octets d'adresse 0x4096 à 0x40d5 
// soit 0x4096 + 0x40 (64 en hexadécimal)
_mm_clflush((void const *) 0x4096);

// Lire la valeur sur 64 bits du compteur de cycles pour le processeur en cours.
// Le paramètre junk n'est pas nécessaire mais doit être un pointeur valide.
uint32_t junk;
uint64_t timestamp = __rdtscp(&junk);
```

> La variable `junk` est nécessaire pour que la fonction `__rdtscp` retourne le bon résultat (elle contient l'id du cœur sur lequel le code est exécuté). On ne s'en servira pas.

### Expérimentation 1. Mesure du temps d'accès au cache

Fichier à compléter / compiler : `cachetime.c`

Complétez `cachetime.c` pour mesurer les différences de temps d'accès entre des données dans le cache et hors cache.
Exécutez le programme une dizaine de fois, en déduire un seuil approximatif qui différencie ces deux temps d'accès.
On se servira de ce seuil pour déterminer si une donnée est dans le cache ou non.

### Expérimentation 2. Le cache comme canal auxiliaire

Fichier à compléter / compiler : `flushreload.c`

L'idée est d'utiliser le temps d'accès au cache pour révéler un secret.
On suppose une fonction `victim` qui utilise le secret comme index pour accéder à un tableau. Ce secret est
supposé inaccessible. La technique qu'on utilise pour révéler le secret manipulé par `victim` est appelée FLUSH+RELOAD.
Le principe est le suivant :

1. Vider complètement le tableau de la mémoire cache (partie FLUSH).
2. Appeler la victime. Cette action provoque le chargement dans le cache d'une ligne qui dépend du secret.
3. Recharger le tableau dans le cache et mesurer le temps de rechargement de chaque élément. Si le temps est court, l'élément était déjà dans le cache et c'est cet élément que la victime a utilisé : on connaît alors le secret (partie RELOAD).

- Complétez `flushreload.c`
- Pourquoi ne pas utiliser simplement `array[256]`, c'est à dire un tableau de 256 octets ?
- Pourquoi utiliser un décalage dans les blocs de mémoire (rôle de `DELTA`) ?
- Est ce que la technique est fiable à 100% ? Pourquoi ?

## Deuxième partie. Exécution spéculative et prédiction de branchements

Fichier à compléter / compiler : `spectreexperiment.c`

Spectre repose sur l'exécution spéculative et la prédiction de branchement, qui est sont des optimisations importantes utilisée dans tous les CPU modernes
pour maximiser le taux d'utilisation de ses unités de calcul (et donc maximiser les performances d'exécution).

### Exécution spéculative et prédictions de branchements

Pour rendre l'exécution plus efficace, lorsqu'ils rencontrent un branchement dans le programme les CPU commencent l'exécution
à l'adresse la plus probable de branchement. Cette adresse est déterminée de manière :

- Statique si l'instruction n'a jamais été vue - Saut supposé pris s'il est vers l'arrière du programme (cas des boucles), non pris s'il est vers l'avant (cas des alternatives, la clause 'alors' est jugée plus probable que la clause 'sinon')
- Dynamique si l'instruction a déjà été vue - Saut supposé pris ou non pris en fonction des exécutions antérieures

Les pipelines du cœur commencent à lire, décoder et exécuter instructions qui se trouvent à la destination du saut sans impact sur son état
architectural (visible par le programme). Lors de la détermination finale de la destination du saut, si la prédiction est juste, ces
instructions sont validées. Sinon, l'état interne du cœur est réinitialisé à ce qu'il était avant l'instruction de saut. Le nombre d'instructions
qu'il est possible d'exécuter spéculativement dépend de la taille du *Reorder Buffer* de chaque cœur et est difficile à connaître précisément
(quelques centaines en général).

### Exécution dans le désordre

Les CPU modernes déterminent automatiquement les dépendances de données entre instructions successives. Les instructions qui sont jugées
indépendantes sont ordonnancées pour exécution directement, celles jugées dépendantes sont mises en attente de disponibilité
de leurs opérandes. Ce mécanisme conduit à exécuter les instructions possiblement dans le désordre. Le but est de ne pas attendre d'avoir
fini une instruction à latence élevée (par exemple un accès à la mémoire centrale) pour commencer à exécuter les instructions qui les suivent mais ne dépendent
pas d'elles dans le programme. Ainsi, des instructions ultérieures peuvent s'exécuter alors qu'une instruction qui les précède attend un résultat.
Pour donner l'illusion que le programme s'exécute dans l'ordre de ses instructions, un buffer interne au cœur (le *Reorder Buffer*, ROB) enregistre
toutes les instructions en cours d'exécution en les gardant classées dans l'ordre du programme. La plus ancienne, quand elle est supprimée du ROB
une fois son exécution terminée, détermine l'état architectural du cœur pour ce cycle. On dit que cette instruction est *retired*.
**Les exceptions ne sont déclenchées qu'au moment du retrait de l'instruction, pas de son exécution.**

### Cache et exécution spéculative

Lors des accès à la mémoire, le cache est bien sûr impacté : ces accès sont effectifs puisqu'il faut bien connaître les valeurs des
opérandes des instructions pour pouvoir les exécuter, même spéculativement. Les données mémoire liées à une instruction seront donc
amenées dans le cache même si, finalement, cette instruction n'est pas validée dans l'état architectural du cœur (et donc dans l'état *réel*
du programme).

### Expérimentation 3. Impact de la prédiction de branchements et de l'exécution dans le désordre

 1. Exécuter le programme (plusieurs fois). Qu'observez-vous ?
 2. Ligne 89, remplacer par `victim(i+20);`. Que se passe-t-il ? Pourquoi ? Remettez le code initial.
 3. Commentez les lignes 84, 87, 94 et 97 (flush de size). Est-ce que l'attaque fonctionne toujours ? Pourquoi ?
 4. Décommentez les lignes 84, 87, 94 et 97.

## Troisième partie. Attaque 'Spectre'

Fichier à compléter / compiler : `spectreattack.c`

Dans ce code, on suppose une fonction `restrictedAccess` qui permet l'accès à un buffer limité pour simuler une sandbox.
Le secret est en  dehors de la sandbox. L'accès au secret n'est possible qu'au travers de la fonction `restrictedAccess`
(c'est à dire qu'on a pas le droit de le lire directement). Le but est d'afficher le secret.

### Expérimentation 4. Premier essai

 1. Exécutez le programme et déterminez si vous êtes capable de déterminer la valeur secrète. Pourquoi le résultat n'est
 pas fiable ? Exécutez le programme un nombre suffisant de fois pour être sûr du secret.

### Expérimentation 5. Amélioration de la fiabilité de l'attaque

 Fichier à compléter / compiler : `spectreattackimproved.c`

 L'idée est d'automatiser la répétition de l'attaque et de maintenir un tableau de scores pour chaque succès de cache.
 C'est une approche statistique. Le secret est supposé être la valeur pour laquelle on a eu le plus de succès de cache.

 1. Exécutez le programme. Qu'observez vous quant à la fiabilité de l'attaque ?

### Expérimentation 6. Détermination de tout le secret

Copiez `spectreattackimproved.c` en `spectreallsecret.c`.
Modifiez `spectreallsecret.c` pour qu'il retourne tout le secret (toute la chaîne de caractère).

### Expérimentation 7. Contre-mesure

La fonction `__mm_mfence` [doc intel](https://software.intel.com/sites/landingpage/IntrinsicsGuide/#text=_mm_mfence) permet
d'implémenter une contre-mesure efficace en l'appelant à l'endroit approprié dans `restrictedAccess`.

1. Où faudrait-il introduire cette fonction ?
2. Modifiez `spectreattackimproved.c` en conséquence et vérifiez que la sandbox n'est plus vulnérable à l'attaque.
