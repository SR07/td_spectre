
# Noms des fichiers sources et des exécutables
SOURCES = cachetime.c flushreload.c spectreattack.c spectreattackimproved.c spectreexperiment.c spectreallsecret.c
EXECUTABLES = $(SOURCES:.c=)

# Options de compilation
CC = gcc
CFLAGS = -Wall -O0 -g

# Règles de compilation pour chaque exécutable
$(EXECUTABLES): %: %.c
	$(CC) $(CFLAGS) -o $@ $<

# Règle pour compiler tous les exécutables
all: $(EXECUTABLES)

# Règle pour supprimer tous les fichiers objets et exécutables
clean:
	rm -f $(EXECUTABLES)
